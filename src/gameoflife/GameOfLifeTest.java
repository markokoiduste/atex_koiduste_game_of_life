package gameoflife;


import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Test;

public class GameOfLifeTest {

    // Võiks alustada sellest, et kaadris on võimalik rakke elusaks märkida.

    // Siis võiks proovida naabreid lugeda

    // Siis võiks järgmist kaadrit arvutada.
    // Esialgu mõnda väga lihtsat.

    // Kui vajate abimeetodeid Nt. Frame.toString() siis kirjutage testid ka neile.

    // Kui kõik on valmis siis peaks testid stillWorksCorrectly(),
    // pulsarWorksCorrectly() ja gliderWorksCorrectly() töötama.
    // Soovitan esialgu neile pigem tähelepanu mitte pöörata ja teha asja sammhaaval.
    // Neid kontrollige alles siis, kui töö enamvähem valmis.

    @Test
    public void stillWorksCorrectly() {
        Frame frame = getFrame("------",
                               "--XX--",
                               "-X--X-",
                               "--XX--",
                               "------");


        assertThat(frame.nextFrame(), is(equalTo(frame)));
    }

    @Test
    public void pulsarWorksCorrectly() {
        Frame frame = getFrame("------",
                               "-XX---",
                               "-X----",
                               "----X-",
                               "---XX-",
                               "------");

        Frame expected = getFrame("------",
                                  "-XX---",
                                  "-XX---",
                                  "---XX-",
                                  "---XX-",
                                  "------");

        assertThat(frame.nextFrame(), is(equalTo(expected)));
        assertThat(frame.nextFrame().nextFrame(), is(equalTo(frame)));
    }

    @Test
    public void gliderWorksCorrectly() {
        Frame frame1 = getFrame("-X----",
                                "--XX--",
                                "-XX---",
                                "------");

        Frame frame2 = getFrame("--X---",
                                "---X--",
                                "-XXX--",
                                "------");

        Frame frame3 = getFrame("------",
                                "-X-X--",
                                "--XX--",
                                "--X---");

        assertThat(frame1.nextFrame(), is(equalTo(frame2)));
        assertThat(frame2.nextFrame(), is(equalTo(frame3)));
    }

    private Frame getFrame(String ... rows) {

        // abimeetod, mis loob stringide massiivist Frame objekti
        // See meetod on siin vaid selleks, et saaks teste mugavamalt kirja panna.

        return null;
    }
}